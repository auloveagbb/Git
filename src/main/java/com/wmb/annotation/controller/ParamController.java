package com.wmb.annotation.controller;

import com.wmb.annotation.annotation.ParamModel;
import com.wmb.annotation.model.User;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wulijin
 */
@RestController
public class ParamController {

    @PostMapping("/test1")
    public String getAll(@ParamModel User user) {
        System.out.println(user.getUserName());
        return "xyx";
    }
}
