package com.wmb.annotation.controller;

import com.wmb.annotation.model.User;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wulijin
 */
@RestController
public class XmlController {

    @RequestMapping(value = "/test2", produces = MediaType.APPLICATION_XML_VALUE)
    public User toXml() {
        return new User("小红", 11, "上海");
    }
}
