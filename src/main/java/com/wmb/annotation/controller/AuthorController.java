package com.wmb.annotation.controller;

import com.wmb.annotation.annotation.Author;
import com.wmb.annotation.annotation.Test;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wulijin
 */
@RestController
@RequestMapping("/test")
public class AuthorController {

    @RequestMapping("/t1")
    @Author
    public String getAuthor() {
        return "ok";
    }

    @RequestMapping("/t2")
    @Test(value = "{12123131}")
    @Author
    public String test() {
        return "test";
    }
}
