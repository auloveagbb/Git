package com.wmb.annotation.interceptor;

import com.wmb.annotation.annotation.Author;
import com.wmb.annotation.utils.AuthorUtil;
import java.lang.reflect.Method;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 * @author wulijin
 */
@Component
public class AuthorInterceptor extends HandlerInterceptorAdapter {

    @Autowired
    private AuthorUtil authorUtil;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)  {
        HandlerMethod handlerMethod = (HandlerMethod) handler;
        Method method = handlerMethod.getMethod();
        Author author = method.getAnnotation(Author.class);
        if (author != null) {
            authorUtil.getAuthor();
        }
        return true;
    }
}
