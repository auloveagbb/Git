package com.wmb.annotation.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author wulijin
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@XStreamAlias("user")
@JacksonXmlRootElement(localName = "base")
public class User implements Serializable {
    private static final long serialVersionUID = 1780877470661472709L;
    @XStreamAsAttribute
    private String userName;
    @XStreamAsAttribute
    private int age;
    @XStreamAsAttribute
    private String address;
}
