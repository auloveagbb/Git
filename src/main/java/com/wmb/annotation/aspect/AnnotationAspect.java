package com.wmb.annotation.aspect;

import com.wmb.annotation.annotation.Test;
import java.lang.reflect.Method;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

/**
 * @author wulijin
 */
@Component
@Aspect
@Slf4j
public class AnnotationAspect {


    @Pointcut("@annotation(com.wmb.annotation.annotation.Test)")
    public void annoTest() {
    }

    @Before("annoTest()")
    public void before(JoinPoint joinPoint) {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        Test test = method.getAnnotation(Test.class);
        System.out.println("打印" + test.value() + "之前");
    }

    @Around("annoTest()")
    public Object advice(ProceedingJoinPoint joinPoint) {
        System.out.println("通知开始之前");
        Object msg = null;
        try {
            msg = joinPoint.proceed();
            System.out.println("===========" + msg);
        } catch (Throwable e) {
            log.error("{}", e.getMessage());
        }
        System.out.println("通知结束");
        return msg;
    }

    @After("annoTest()")
    public void after() {
        System.out.println("after方法执行后");
    }

}
