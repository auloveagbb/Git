package com.wmb.annotation.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author wulijin
 */
@Component
@Slf4j
public class AuthorUtil {

    public void getAuthor() {
        System.out.println("wulijin");
    }
}
